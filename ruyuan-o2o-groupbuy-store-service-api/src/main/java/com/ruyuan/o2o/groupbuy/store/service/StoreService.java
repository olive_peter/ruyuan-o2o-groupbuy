package com.ruyuan.o2o.groupbuy.store.service;

import com.ruyuan.o2o.groupbuy.store.vo.StoreVO;

import java.util.List;

/**
 * 门店服务service组件接口
 *
 * @author ming qian
 */
public interface StoreService {
    /**
     * 更新门店
     *
     * @param storeVO 门店vo
     * @return 更新结果
     */
    Boolean update(StoreVO storeVO);

    /**
     * 分页查询门店
     *
     * @param pageNum  查询页数
     * @param pageSize 每次查询数
     * @return 查询列表
     */
    List<StoreVO> listByPage(Integer pageNum, Integer pageSize);

    /**
     * 根据id查询门店
     *
     * @param storeId 门店id
     * @return 门店vo
     */
    StoreVO findById(Integer storeId);
}
