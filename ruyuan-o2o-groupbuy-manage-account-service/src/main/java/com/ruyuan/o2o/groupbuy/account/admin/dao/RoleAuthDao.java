package com.ruyuan.o2o.groupbuy.account.admin.dao;

import com.ruyuan.o2o.groupbuy.account.model.RoleAuthModel;
import org.springframework.stereotype.Repository;

/**
 * 角色绑定权限DAO
 *
 * @author ming qian
 */
@Repository
public interface RoleAuthDao {

    /**
     * 角色绑定权限
     *
     * @param roleAuthModel 绑定关系model
     * @return
     */
    Boolean roleBindAuth(RoleAuthModel roleAuthModel);

}
