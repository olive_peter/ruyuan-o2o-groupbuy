package com.ruyuan.o2o.groupbuy.coupons.admin.dao;

import com.ruyuan.o2o.groupbuy.coupons.model.CouponsModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 优惠券服务DAO
 *
 * @author ming qian
 */
@Repository
public interface CouponsDao {
    /**
     * 保存优惠券
     *
     * @param couponsModel
     */
    void save(CouponsModel couponsModel);

    /**
     * 分页查询门店
     *
     * @return
     */
    List<CouponsModel> listByPage();

}