package com.ruyuan.o2o.groupbuy.account.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 运营端平台商户账号实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class AccountModel implements Serializable {
    private static final long serialVersionUID = 246590433964509991L;

    /**
     * 管理用户id
     */
    private Integer adminId;

    /**
     * 管理用户名称
     */
    private String adminName;

    /**
     * 手机号码
     */
    private Integer mobile;

    /**
     * 密码
     */
    private String passwd;

    /**
     * 角色id
     */
    private Integer roleId;

    /**
     * 最后登录时间
     */
    private String lastLogin;

    /**
     * 最后登录ip
     */
    private String lastIp;

    /**
     * 账号是否关闭 0 正常 1 关闭
     */
    private Integer closed;

    /**
     * 账号创建时间
     */
    private String createDate;
}
