package com.ruyuan.o2o.groupbuy.promotions.service;

import com.ruyuan.o2o.groupbuy.promotions.model.PromotionsModel;
import com.ruyuan.o2o.groupbuy.promotions.vo.PromotionsVO;

import java.util.List;

/**
 * 活动服务service组件接口
 *
 * @author ming qian
 */
public interface PromotionsService {
    /**
     * 创建活动
     *
     * @param promotionsVO
     */
    void save(PromotionsVO promotionsVO);

    /**
     * 更新活动
     *
     * @param promotionsVO
     */
    void update(PromotionsVO promotionsVO);

    /**
     * 删除活动
     *
     * @param promotionId
     */
    void delete(Integer promotionId);

    /**
     * 分页查询活动
     *
     * @param pageNum  查询页数
     * @param pageSize 每次查询数
     * @return 查询列表
     */
    List<PromotionsVO> listByPage(Integer pageNum, Integer pageSize);

    /**
     * 根据id查询活动
     *
     * @param promotionId 活动id
     * @return 活动vo
     */
    PromotionsVO findById(Integer promotionId);

    /**
     * 查询当前运行中活动
     *
     * @return 活动model
     */
    PromotionsModel findRunningPromotion();

}
