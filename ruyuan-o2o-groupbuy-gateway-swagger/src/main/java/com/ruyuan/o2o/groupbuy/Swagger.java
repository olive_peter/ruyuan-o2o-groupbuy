package com.ruyuan.o2o.groupbuy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * swagger地址：http://服务器ip:8769/swagger-ui.html
 *
 * @author ming qian
 */
@SpringBootApplication
@EnableDiscoveryClient
public class Swagger {
    public static void main(String[] args) {
        SpringApplication.run(Swagger.class, args);
    }
}
