package com.ruyuan.o2o.groupbuy.points.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 积分服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class PointsModel implements Serializable {
    private static final long serialVersionUID = -5931181926682542609L;

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 消费者用户id
     */
    private Integer userId;

    /**
     * 本次增加积分
     */
    private Integer pointsAdd;

    /**
     * 总积分
     */
    private Integer pointsTotal;

    /**
     * 支付id
     */
    private Integer payId;
}