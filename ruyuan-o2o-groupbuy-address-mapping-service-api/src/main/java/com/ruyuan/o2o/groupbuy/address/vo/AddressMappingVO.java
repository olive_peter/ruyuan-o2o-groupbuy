package com.ruyuan.o2o.groupbuy.address.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 省市区映射服务VO类
 *
 * @author ming qian
 */
@ApiModel(value = "省市区映射服务VO类")
@Getter
@Setter
@ToString
public class AddressMappingVO implements Serializable {
    private static final long serialVersionUID = 6135058085468783895L;

    /**
     * 省id
     */
    @ApiModelProperty("省id")
    private Integer provinceId;

    /**
     * 省名称
     */
    @ApiModelProperty("省id")
    private String provinceName;

    /**
     * 市id
     */
    @ApiModelProperty("市id")
    private Integer cityId;

    /**
     * 市名称
     */
    @ApiModelProperty("市名称")
    private String cityName;

    /**
     * 区id
     */
    @ApiModelProperty("区id")
    private Integer districtId;

    /**
     * 区名称
     */
    @ApiModelProperty("区名称")
    private String districtName;

    /**
     * 管理用户id
     */
    @ApiModelProperty("管理用户id")
    private Integer adminId;
}