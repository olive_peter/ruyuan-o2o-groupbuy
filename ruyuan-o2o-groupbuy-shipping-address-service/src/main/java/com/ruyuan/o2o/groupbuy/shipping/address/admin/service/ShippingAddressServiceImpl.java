package com.ruyuan.o2o.groupbuy.shipping.address.admin.service;

import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.shipping.address.admin.dao.ShippingAddressDao;
import com.ruyuan.o2o.groupbuy.shipping.address.model.ShippingAddressModel;
import com.ruyuan.o2o.groupbuy.shipping.address.service.ShippingAddressService;
import com.ruyuan.o2o.groupbuy.shipping.address.vo.ShippingAddressVO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 处理用户收货地址服务service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = ShippingAddressService.class, cluster = "failfast", loadbalance = "roundrobin")
public class ShippingAddressServiceImpl implements ShippingAddressService {

    @Autowired
    private ShippingAddressDao shippingAddressDao;

    /**
     * 保存用户收货地址
     *
     * @param shippingAddressVO
     */
    @Override
    public void save(ShippingAddressVO shippingAddressVO) {
        ShippingAddressModel shippingAddressModel = new ShippingAddressModel();
        BeanMapper.copy(shippingAddressVO, shippingAddressModel);
        shippingAddressModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        shippingAddressDao.save(shippingAddressModel);
    }
}
