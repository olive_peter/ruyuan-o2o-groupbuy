package com.ruyuan.o2o.groupbuy.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 批处理服务启动类
 *
 * @author ming qian
 */
@SpringBootApplication
@EnableDiscoveryClient
public class BatchMain {
  public static void main(String[] args) {
    SpringApplication.run(BatchMain.class, args);
  }
}
