package com.ruyuan.o2o.groupbuy.account.admin.service;

import com.github.pagehelper.PageHelper;
import com.ruyuan.o2o.groupbuy.account.admin.dao.AccountDao;
import com.ruyuan.o2o.groupbuy.account.admin.dao.AccountRoleDao;
import com.ruyuan.o2o.groupbuy.account.model.AccountModel;
import com.ruyuan.o2o.groupbuy.account.model.AccountRoleModel;
import com.ruyuan.o2o.groupbuy.account.service.AccountService;
import com.ruyuan.o2o.groupbuy.account.vo.AccountRoleVO;
import com.ruyuan.o2o.groupbuy.account.vo.AccountVO;
import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * 处理管理账号增删改查service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = AccountService.class, cluster = "failfast", loadbalance = "roundrobin")
@Slf4j
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDao accountDao;

    @Autowired
    private AccountRoleDao accountRoleDao;

    private final String SYSTEM_ACCOUNT_NAME = "system";

    /**
     * 创建账号
     *
     * @param accountVO 前台表单
     * @return
     */
    @Override
    public void save(AccountVO accountVO) {
        AccountModel accountModel = new AccountModel();
        BeanMapper.copy(accountVO, accountModel);
        accountModel.setLastIp("127.0.0.1");
        accountModel.setLastLogin(TimeUtil.format(System.currentTimeMillis()));
        accountModel.setCreateDate(TimeUtil.format(System.currentTimeMillis()));
        accountDao.save(accountModel);
    }

    /**
     * 更新账号
     *
     * @param accountVO 前台表单
     * @return
     */
    @Override
    public void update(AccountVO accountVO) {
        AccountModel accountModel = new AccountModel();
        BeanMapper.copy(accountVO, accountModel);
        accountModel.setLastLogin(TimeUtil.format(System.currentTimeMillis()));
        accountDao.update(accountModel);
    }

    /**
     * 关闭账号
     *
     * @param adminId 账号id
     * @return
     */
    @Override
    public Boolean close(Integer adminId) {
        String adminName = null;
        try {
            adminName = accountDao.findById(adminId).getAdminName();
        } catch (Exception e) {
            log.error("用户不存在");
            return Boolean.FALSE;
        }
        if (null == adminName || !adminName.equals(SYSTEM_ACCOUNT_NAME)) {
            log.error("当前账号无关闭权限");
        }
        accountDao.close(adminId);
        return Boolean.TRUE;
    }

    /**
     * 开启账号
     *
     * @param adminId 账号id
     * @return
     */
    @Override
    public Boolean open(Integer adminId) {
        String adminName = null;
        try {
            adminName = accountDao.findById(adminId).getAdminName();
        } catch (Exception e) {
            log.error("用户不存在");
            return Boolean.FALSE;
        }
        if (null == adminName || !adminName.equals(SYSTEM_ACCOUNT_NAME)) {
            log.error("当前账号无开启权限");
        }
        accountDao.open(adminId);
        return Boolean.TRUE;
    }

    /**
     * 分页查询账号
     *
     * @param pageNum  页数
     * @param pageSize 每页展示数据数量
     * @return
     */
    @Override
    public List<AccountVO> listByPage(Integer pageNum, Integer pageSize) {
        List<AccountVO> accountVoS = new ArrayList<>();

        PageHelper.startPage(pageNum, pageSize);
        List<AccountModel> accountModels = accountDao.listByPage();

        for (AccountModel accountModel : accountModels) {
            AccountVO accountVO = new AccountVO();
            BeanMapper.copy(accountModel, accountVO);
            accountVoS.add(accountVO);
        }

        return accountVoS;
    }

    /**
     * 根据id查询账号
     *
     * @param adminId 账号id
     * @return
     */
    @Override
    public AccountVO findById(Integer adminId) {
        AccountModel accountModel = accountDao.findById(adminId);
        if (null == accountModel) {
            return null;
        }
        AccountVO accountVO = new AccountVO();
        BeanMapper.copy(accountModel, accountVO);
        return accountVO;
    }

    /**
     * 用户绑定角色
     *
     * @param accountRoleVO 前台表单
     * @return
     */
    @Override
    public boolean accountBindRole(AccountRoleVO accountRoleVO) {
        AccountRoleModel accountRoleModel = new AccountRoleModel();
        BeanMapper.copy(accountRoleVO, accountRoleModel);
        accountRoleDao.accountBindRole(accountRoleModel);
        return Boolean.TRUE;
    }

}
