package com.ruyuan.o2o.groupbuy.user.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 消费者会员用户服务VO类
 *
 * @author ming qian
 */
@ApiModel(value = "会员用户服务VO类")
@Getter
@Setter
@ToString
public class UserVO implements Serializable {
    private static final long serialVersionUID = 4975606668624988904L;

    /**
     * 主键id
     */
    @ApiModelProperty("门店id")
    private Integer id;

    /**
     * 消费者用户id
     */
    @ApiModelProperty("门店id")
    private Integer userId;

    /**
     * 用户手机号
     */
    @ApiModelProperty("门店id")
    private String userPhone;

    /**
     * 门店id
     */
    @ApiModelProperty("门店id")
    private Integer storeId;

    /**
     * 会员状态 0 未充值注册成为会员 1 已充值注册成为会员
     */
    @ApiModelProperty("门店id")
    private Integer memberStatus;

    /**
     * 创建时间
     */
    @ApiModelProperty("门店id")
    private String createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty("门店id")
    private String updateTime;
}