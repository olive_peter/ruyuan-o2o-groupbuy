package com.ruyuan.o2o.groupbuy.coupons.admin.dao;

import com.ruyuan.o2o.groupbuy.coupons.model.CouponsReceiveModel;
import org.springframework.stereotype.Repository;

/**
 * 用户领取优惠券服务DAO
 *
 * @author ming qian
 */
@Repository
public interface CouponsReceiveDao {
    /**
     * 用户领取优惠券
     *
     * @param couponsReceiveModel
     */
    void save(CouponsReceiveModel couponsReceiveModel);

}