package com.ruyuan.o2o.groupbuy.account.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 商户权限VO类
 *
 * @author ming qian
 */
@ApiModel(value = "商户权限VO类")
@Getter
@Setter
@ToString
public class AuthVO implements Serializable {
    private static final long serialVersionUID = 1904762744149870638L;

    /**
     * 权限id
     */
    @ApiModelProperty("权限id")
    private Integer authId;

    /**
     * 权限名称
     */
    @ApiModelProperty("权限名称")
    private String authName;
    /**
     * 权限url
     */
    @ApiModelProperty("权限url")
    private String authUrl;
    /**
     * 父权限id
     */
    @ApiModelProperty("父权限id")
    private Integer parentId;
    /**
     * 权限创建时间
     */
    @ApiModelProperty("权限创建时间")
    private String createTime;
    /**
     * 权限更新时间
     */
    @ApiModelProperty("权限更新时间")
    private String updateTime;
}
