## 儒猿互联网O2O团购系统的20+微服务拆分实战

定位: o2o团购系统工业级20+微服务拆分实战。

### 内容说明：

本项目是**儒猿架构**《石杉架构课》的 《阶段三：大规模微服务架构设计与生产实战项目：1、互联网O2O团购系统的20+微服务拆分实战》, 版权归儒猿技术窝所有，侵权将追究法律责任

架构课程详细信息

- [儒猿架构官网](http://www.ruyuan2020.com/)
- [石杉架构课程大纲](https://docs.qq.com/pdf/DY3dHR2RDTG9CUU91)
- [石杉架构课程B站直通车](https://space.bilibili.com/478364560/channel/detail?cid=155048)

## 公众号:儒猿技术窝

更多技术干货，请扫描下方二维码，关注公众号儒猿技术窝

![ruyuan](https://gitee.com/shishan100/Java-Interview-Advanced/raw/master/images/1831593704931_.pic_hd.jpg) 