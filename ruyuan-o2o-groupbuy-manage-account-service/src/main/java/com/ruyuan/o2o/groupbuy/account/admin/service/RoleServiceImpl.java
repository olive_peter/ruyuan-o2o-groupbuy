package com.ruyuan.o2o.groupbuy.account.admin.service;

import com.github.pagehelper.PageHelper;
import com.ruyuan.o2o.groupbuy.account.admin.dao.RoleAuthDao;
import com.ruyuan.o2o.groupbuy.account.model.RoleAuthModel;
import com.ruyuan.o2o.groupbuy.account.model.RoleModel;
import com.ruyuan.o2o.groupbuy.account.service.RoleService;
import com.ruyuan.o2o.groupbuy.account.vo.RoleAuthVO;
import com.ruyuan.o2o.groupbuy.account.vo.RoleVO;
import com.ruyuan.o2o.groupbuy.account.admin.dao.RoleDao;
import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 处理管理角色增删改查service组件
 *
 * @author ming qian
 */
@Service
@Slf4j
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private RoleAuthDao roleAuthDao;

    /**
     * 创建角色
     *
     * @param roleVO
     */
    @Override
    public void save(RoleVO roleVO) {
        RoleModel roleModel = new RoleModel();
        BeanMapper.copy(roleVO, roleModel);
        roleDao.save(roleModel);
    }

    /**
     * 更新角色
     *
     * @param roleVO
     */
    @Override
    public void update(RoleVO roleVO) {
        RoleModel roleModel = new RoleModel();
        BeanMapper.copy(roleVO, roleModel);
        roleDao.update(roleModel);
    }

    /**
     * 分页查询
     *
     * @return
     */
    @Override
    public List<RoleVO> listByPage(Integer pageNum, Integer pageSize) {
        List<RoleVO> roleVoS = new ArrayList<>();

        PageHelper.startPage(pageNum, pageSize);
        List<RoleModel> roleModels = roleDao.listByPage();

        for (RoleModel roleModel : roleModels) {
            RoleVO roleVO = new RoleVO();
            BeanMapper.copy(roleModel, roleVO);
            roleVoS.add(roleVO);
        }

        return roleVoS;
    }

    /**
     * 根据id查询角色
     *
     * @param roleId 角色id
     * @return
     */
    @Override
    public RoleVO findById(Integer roleId) {
        RoleModel roleModel = roleDao.findById(roleId);
        RoleVO roleVO = new RoleVO();
        BeanMapper.copy(roleModel, roleVO);
        return roleVO;
    }

    /**
     * 角色绑定权限
     *
     * @param roleAuthVO 前台表单
     * @return
     */
    @Override
    public boolean roleBindAuth(RoleAuthVO roleAuthVO) {
        RoleAuthModel roleAuthModel = new RoleAuthModel();
        BeanMapper.copy(roleAuthVO, roleAuthModel);
        roleAuthDao.roleBindAuth(roleAuthModel);
        return Boolean.TRUE;
    }
}
