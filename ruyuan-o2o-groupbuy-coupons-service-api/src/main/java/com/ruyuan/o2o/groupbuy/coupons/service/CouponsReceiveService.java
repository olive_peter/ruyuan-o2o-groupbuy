package com.ruyuan.o2o.groupbuy.coupons.service;


import com.ruyuan.o2o.groupbuy.coupons.vo.CouponsReceiveVO;

/**
 * 用户领取优惠券service组件接口
 *
 * @author ming qian
 */
public interface CouponsReceiveService {

    /**
     * 用户领取优惠券
     *
     * @param couponsReceiveVO 用户领取优惠券vo
     * @return 领取结果
     */
    Boolean receiveCoupons(CouponsReceiveVO couponsReceiveVO);
}
