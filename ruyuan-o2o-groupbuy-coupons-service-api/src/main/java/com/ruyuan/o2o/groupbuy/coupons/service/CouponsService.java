package com.ruyuan.o2o.groupbuy.coupons.service;


import com.ruyuan.o2o.groupbuy.coupons.vo.CouponsVO;

import java.util.List;

/**
 * 优惠券服务service组件接口
 *
 * @author ming qian
 */
public interface CouponsService {

    /**
     * 创建优惠券
     *
     * @param couponsVO 优惠券vo
     * @return 创建结果
     */
    Boolean save(CouponsVO couponsVO);

    /**
     * 分页查询优惠券
     *
     * @param pageNum  查询页数
     * @param pageSize 每次查询数
     * @return 查询列表
     */
    List<CouponsVO> listByPage(Integer pageNum, Integer pageSize);

}
